/*
 * USBD_User_HID_0.h
 *
 *  Created on: 2 Jan 2019
 *      Author: OJCIEC
 */

#ifndef USBD_USER_HID_0_H_
#define USBD_USER_HID_0_H_

#include <stdint.h>
#include "DAP_config.h"

extern uint16_t USB_RequestIndexI;      // Request  Index In
extern uint16_t USB_RequestIndexO;      // Request  Index Out
extern uint16_t USB_RequestCountI;      // Request  Count In
extern uint16_t USB_RequestCountO;      // Request  Count Out

extern uint16_t USB_ResponseIndexI;     // Response Index In
extern uint16_t USB_ResponseIndexO;     // Response Index Out
extern uint16_t USB_ResponseCountI;     // Response Count In
extern uint16_t USB_ResponseCountO;     // Response Count Out
extern uint8_t  USB_ResponseIdle;       // Response Idle  Flag

extern uint8_t  USB_Request [DAP_PACKET_COUNT][DAP_PACKET_SIZE];  // Request  Buffer
extern uint8_t  USB_Response[DAP_PACKET_COUNT][DAP_PACKET_SIZE];  // Response Buffer

void USBD_HID0_Initialize(void);


#endif /* USBD_USER_HID_0_H_ */
