/*
 * gpio.h
 *
 *  Created on: 31 Dec 2018
 *      Author: OJCIEC
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "stm32f1xx.h"

#define INLINE	inline __attribute__((always_inline))

#define TCK_SWCLK_PORT		GPIOA
#define TCK_SWCLK_PIN		0
#define SWD_SCK_PORT 		TCK_SWCLK_PORT
#define SWD_SCK_PIN			TCK_SWCLK_PIN

#define TMS_SWDIO_PORT		GPIOA
#define TMS_SWDIO_PIN		1
#define SWD_SWD_PORT		TMS_SWDIO_PORT
#define SWD_SWD_PIN			TMS_SWDIO_PIN

#define TDI_PORT			GPIOA
#define TDI_PIN				2

#define TDO_PORT			GPIOA
#define TDO_PIN				3

#define nTRST_PORT			GPIOA
#define nTRST_PIN			4

#define nRESET_PORT			GPIOA
#define nRESET_PIN			5
#define SWD_NRST_PORT		nRESET_PORT
#define SWD_NRST_PIN		nRESET_PIN

#define LED_PORT			GPIOC
#define LED_PIN				13

typedef enum
{
	DIGITAL_OUT_PUSH_PULL = 0b0011,
	DIGITAL_OUT_OPEN_DRAIN = 0b0111,

	INPUT_ANALOG = 0b0000,
	INPUT_FLOATING = 0b0100,
	INPUT_PUSH_PULL = 0b1000,
}GPIO_TYPE_T;


static INLINE void GPIO_Config(GPIO_TypeDef *gpio, int pin, GPIO_TYPE_T type, int level)
{
	volatile uint32_t *CR = (pin < 8) ? &gpio -> CRL : &gpio -> CRH;
	unsigned position = pin & 7;

	*CR &= ~(0b1111UL << (position * 4));
	*CR |= type << (position * 4);

	if(level < 2) gpio -> BSRR = (1 << (pin + !(level) * 16));
}

static INLINE void GPIO_Set(GPIO_TypeDef *gpio, int pin, int level)
{
	gpio -> BSRR = (1 << (pin + !(level) * 16));
}

static INLINE void GPIO_Hi(GPIO_TypeDef *gpio, int pin)
{
	gpio -> BSRR = (1 << pin);
}

static INLINE void GPIO_Low(GPIO_TypeDef *gpio, int pin)
{
	gpio -> BRR = 1 << pin;
}

static INLINE int GPIO_Get(GPIO_TypeDef *gpio, int pin)
{
	return !!(gpio -> IDR & (1 << pin));
}

static INLINE int GPIO_Get01(GPIO_TypeDef *gpio, int pin)
{
	return !!(gpio -> IDR & (1 << pin));
}



#endif /* GPIO_H_ */
